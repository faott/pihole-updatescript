# pihole-updatescript

Bash Update-Script for the Pihole Adblocker. This script prevented the hangup of the update process/terminal while updating the base OS when a running pihole was present.

## Environment

pihole was setup on a Ubuntu Server LXC container running on a proxmox installation.

## What it does

The script stops the services which are used by pihole.

- Erfolgen einer Statusüberprüfung der betroffenen Services.
- Chcking the status of the affected services
- Checking pihole for updates
- Asking for confirmation to proceed update
- Stopping the services
- Updating the operating system
- Restarting services